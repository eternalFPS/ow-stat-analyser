#!/usr/bin/env python3

from urllib.request import urlopen
from urllib.request import Request
from urllib.parse import quote
from myVars import heros
from bs4 import BeautifulSoup as soup
import sys
import os

def scrapedata(tag, hero):
    #convert hero to the number blizz uses
    heronum = heros[hero]
    url = "https://playoverwatch.com/en-us/career/pc/"+quote(tag)
    print(url)
    #print(hero)
    req = Request(url, headers = {'User-agent': 'my bot 0.1'})
    html = urlopen(req)
    bsObj = soup(html.read(), features="lxml");

    #check to see if profile is private
    visibility = bsObj.find("p",{"class":"masthead-permission-level-text"})
    if(visibility.get_text() != "Public Profile"):
        print("PROFILE NOT PUBLIC")
        return None

    compcont = bsObj.find("div",{"id":"competitive"})
    herocont = compcont.find("div",{"data-category-id":heronum})
    tables = herocont.findAll("tbody",{"class":"DataTable-tableBody"})
    #print(tables)
    #7 tables, but we only care about tables 0, 1, 4, 5
    #also care about 6 (for healing heros)
    #also care about 3 apparently (lucio)
    #wondew how long until it cares about them all
    outlist = []
    aslist = []
    for i in [0,1,3,4,5,6]:
        rawtext = tables[i].get_text(separator="@")
        aslist = rawtext.split("@")
        #aslist = aslist[1:]
        outlist += aslist
    return outlist

def makelist(biglist, hero):

    if(biglist == None):
        return None

    outlist = []

    namef = open("heros/"+hero+".names", "r")
    namel = namef.read().splitlines()
    #go though each namel, finding it in the biglist
    
    #debugint, used to find out what values in our .names file
    #that we were unable to find
    debugint = 0
    elims = 0.0
    deaths = 0.0
    gp = 0.0

    gpindex = biglist.index("Games Played")
    gp = float(biglist[gpindex+1])

    #print(biglist)

    for j, n in enumerate(namel):
        print(n[:-1])
        for i in range(len(biglist)):
            if i%2==1:
                continue
            if n=="E:D ratio":
                outlist.append(elims/deaths)
                debugint += j+1
                print("   found "+n)
                break
            if n==biglist[i] or n[:-1]==biglist[i]:
                if n=="Eliminations*":
                    elims = float(biglist[i+1])
                elif n =="Deaths*":
                    deaths = float(biglist[i+1])
                print("   found "+n)
                debugint += j+1
                datain = biglist[i+1]
                if n[-1] == "*":
                    outlist.append(convertdata(datain, gp))
                else:
                    outlist.append(convertdata(datain, 1.0))
                break

    #if not debugint == 136:
    #    print("debugerror:"+str(debugint-137))
    print(outlist)
    return outlist

def convertdata(data, games):
    #PASS games=1 if you dont wanan divide by games played
    #converts the string to appropirate value in float
    #looks for '%' and ':'
    final = 0.0

    if '%' in data:
        final = float(data[:-1])/100
    elif ':' in data:
        strlist = data.split(':')
        for j in range(len(strlist)):
            final += float(strlist[-(j+1)])*(60**j)
    else:
        final = float(data)

    return final/games

def getlist(btag,heroname):
    return makelist(scrapedata(btag,heroname),heroname)

def scrapeSR(tag, srnum):
    #0 1 2 for tank dps or support

    url = "https://playoverwatch.com/en-us/career/pc/"+quote(tag)
    print(url)
    req = Request(url, headers = {'User-agent': 'my bot 0.1'})
    html = urlopen(req)
    bsObj = soup(html.read(), features="lxml");
    SRs = bsObj.findAll("div",{"class":"competitive-rank-level"})
    SR = SRs[srnum].get_text()
    return int(SR)


if __name__ == '__main__':
    btag = "Muma-11444"
    hero = "wreckingball"
    if len(sys.argv)==3:
        btag=sys.argv[1]
        btag.replace('#','-')
        hero = sys.argv[2]
    biglist = scrapedata(btag,hero)
    print(makelist(biglist,hero))
