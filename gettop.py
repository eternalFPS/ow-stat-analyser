#!/usr/bin/env python3

import scrapeUser3
import sys
import os

hero = "wreckingball"

if len(sys.argv) > 1:
    hero = sys.argv[1]

if not os.path.isfile("heros/"+hero+".btags"):
    sys.exit("no such "+hero+".btags")

f = open("heros/"+hero+".btags", encoding='utf-8', mode="r")
lines = f.readlines()

counter = 0
toplist = []

for l in lines:
    l = l.strip()
    # want to break if its not a valid btag
    if '-' not in l:
        continue
    counter+=1
    #print(""+l+" , "+hero)
    player = scrapeUser3.getlist(l,hero)
    # if the profile was private
    if player == None:
        counter-=1
    elif counter ==1:
        toplist = player
    else:
        for i in range(len(player)):
            toplist[i] += player[i]

f.close()
f = open("heros/"+hero+".topstats", "w+")

for r in range(len(toplist)):
    toplist[r]=toplist[r]/counter
    f.write(str(toplist[r])+"\n")

f.close()
