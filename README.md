# OW Stat analyser

Will \[currently\] give you a list of all your stats, and how good/bad they are.  
Also will highlight your best 3, and worst 3 (and on your worst 3, it will tell you whether you need to increase or decrease the stat)

## Known issues

Dva, symmetra, sombra are all bugged

if it cant find the profile (maybe they changed name or got banned) then it dies (need to manually remove that btag from the heroname.btags to fix)

## How to analyse your stats

you need python3, and whatever packages I used too (I think just beautiful soup)  
Then run:  
```  
python3 compare.py *your_btag* *hero_you_want_analysed*
```

## How it works

it gets the average stats from overbuff, and says that those are stats of a 2500 player  
then it gets the average stats of the top 20 players, and says those are the stats of a 5000 player  
from that, it will see where your stats fit in and give your an SR score accordingly.

## How to update

This will only fetch your stats when you run compare.py. If you want to update the 2500 SR and 5000 SR stats, then you need to run different parts.  
First step is grabbing the average (2500 SR) stats, and the btags of all the best players
```
python3 scrapeBuff.py *hero*
```
Then you need to grab the top (5000 SR) stats.  
Note: you may have to edit the hero.name file located in the heros/ directory, as currently it will break if it comes accross a profile that is private (it doesn't have any way of checking, but this will be implemented in the future).  
Then run:
```
python3 gettop.py *hero*
```

I do have an automatic updater but I wouldn't trust it. Worst case is you get blocked for spamming requests.
