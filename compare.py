#!/user/bin/env python3

#args: btag, hero, options

import scrapeUser3
import sys
import os

srestimate = True

def outLine(nametype, eSR, value):
    out = nametype
    out += ": SR ="
    out += str(eSR)
    out += ", Value="
    out += str(value)
    print(out)

hero = "wreckingball"
#btag, hero
if len(sys.argv) > 2:
    hero = sys.argv[2]

f = open("heros/"+hero+".stats", "r")
lines = f.readlines()

avg = []
for l in lines:
    l = l.strip()
    if ':' in l:
        llist = l.split(":")
        num = 0
        for j in range(len(llist)):
            num += float(llist[-(j+1)])*(60**j)
        avg.append(num)
        continue
    elif "," in l:
        l = l.replace(",", "")
    elif "%" in l:
        l = l.replace("%", "")
        num = float(l)
        num = num/100
        avg.append(num)
        continue
    avg.append(float(l))
f.close()

top = []
f2 = open("heros/"+hero+".topstats", "r")
lines = f2.readlines()

for l in lines:
    l = l.strip()
    top.append(float(l))

f2.close()

#actually now check against player stats
btag = ""
if len(sys.argv)>1:
    btag = sys.argv[1]
player = scrapeUser3.getlist(btag,hero)
f3 = open("heros/"+hero+".names", "r")
linenames = f3.readlines()
#remove "_" if it exists in linenames
if "_\n" in linenames:
    linenames.remove("_\n")

worst = [-1,-1,-1]
best = [-1,-1,-1]

#lists are top, avg, player
eSRlist = []
scorelist = []
for i in range(len(player)):
    A = abs(top[i]-avg[i])
    P = top[i]-player[i]
    score = A/P
    ascore = abs(score)
    eSR = (5000*ascore)/(ascore+1)
    eSR = int(eSR)
    eSRlist.append(eSR)
    scorelist.append(score)

    #find worst and best
    if worst[0] == -1 or eSR < eSRlist[worst[0]]:
        worst[2] = worst[1]
        worst[1] = worst[0]
        worst[0] = i
    elif worst[1] == -1 or eSR < eSRlist[worst[1]]:
        worst[2] = worst[1]
        worst[1] = i
    elif worst[2] == -1 or eSR < eSRlist[worst[2]]:
        worst[2] = i

    if best[0] == -1 or eSR > eSRlist[best[0]]:
        best[2] = best[1]
        best[1] = best[0]
        best[0] = i
    elif best[1] == -1 or eSR > eSRlist[best[1]]:
        best[2] = best[1]
        best[1] = i
    elif best[2] == -1 or eSR > eSRlist[best[2]]:
        best[2] = i

    #generate string to print
    outLine(linenames[i].strip(), eSR, round(player[i],2))

#print best and worst
print()
print("worst stats:")
for w in worst:
    outString = linenames[w].strip() + ": "
    outString += "SR=" +str(eSRlist[w]) + ", Value="
    outString += str(round(player[w],2)) + " "
    if scorelist[w] > 0:
        outString += "(increase)"
    else:
        outString += "(decrease)"
    print(outString)
print("best stats:")
for w in best:
    outString = linenames[w].strip() + ": "
    outString += "SR =" + str(eSRlist[w]) + ", Value="
    outString += str(round(player[w],2))
    print(outString)

f3.close()

if srestimate:
    srtype = int(sys.argv[3])
    realSR = scrapeUser3.scrapeSR(btag,srtype)
    print(realSR)
    #cheeky reuse of scorelist, because thta is already in float value
    outrank = scorelist
    for i in range(len(eSRlist)):
        outrank[i] = 1-abs(realSR-eSRlist[i])/realSR
    print(outrank)

