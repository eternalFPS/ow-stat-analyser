#!/usr/bin/env python3

from urllib.request import urlopen
from urllib.request import Request
from bs4 import BeautifulSoup as soup
import sys
import os

hero = "mccree"

if len(sys.argv) > 1:
    hero = sys.argv[1]

print("./heros/"+hero+".names")

if os.path.isfile("./heros/"+hero+".names"):
    print("hero exists")
elif hero == "all":
    print("not currently implemented")
    sys.exit(0)
else:
    print(""+hero+" is not a valid hero")
    sys.exit(0)

f = open("heros/"+hero+".stats", "w+")
f2 = open("heros/"+hero+".btags", "w+")

req = Request("https://www.overbuff.com/heroes/"+hero, headers = {'User-agent': 'my bot 0.1'})
html = urlopen(req)
bsObj = soup(html.read(), features="lxml");


containers = bsObj.findAll("div",{"class":"stat boxed"})

for container in containers:
    stat = container.find("div",{"class":"value"})
    f.write(stat.text+"\n")

table = bsObj.tbody.findAll("tr")

for t in table:
    link = t.a['href']
    f2.write(link[12:]+'\n')

f2.close()
f.close()
